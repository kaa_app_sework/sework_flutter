import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sework_flutter/screens/loader_screen.dart';
import 'package:sework_flutter/screens/login_screen.dart';
import 'package:sework_flutter/screens/sign_up_screen.dart';

void main() => runApp(Main());

class Main extends StatelessWidget {
  Main({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
      ]);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        '/': (context) => LoaderPage(),
        '/login': (context) => LoginScreen(),
        '/signup' : (context) => SignUpScreen(),
      },
    );
  }
}
