import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class SeworkButton extends StatefulWidget {
  ///
  /// Must be placed in a [Row]
  ///  
  final Color color;
  final double margin;
  final String text;
  final VoidCallback onPressed;
  SeworkButton({@required this.color, this.margin, this.text, this.onPressed});

  @override
  State<StatefulWidget> createState() {
    return _SeworkButtonState();
  }
}

class _SeworkButtonState extends State<SeworkButton> {
  ///
  /// Must be placed in a [Row]
  /// 
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
          margin: EdgeInsets.only(
            top: widget.margin,
            right: widget.margin,
          ),
          child: RaisedButton(
            child: Text(
              widget.text,
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
            ),
            color: widget.color,
            onPressed: widget.onPressed,
          )),
      flex: 1,
    );
  }
}

class SeworkButtonIcon extends StatefulWidget {
  final Color color;
  final String text;
  final Function onPressed;
  final IconData icon;
  final double margin;
  SeworkButtonIcon(
      {@required this.color,
      this.margin,
      this.text,
      this.onPressed,
      this.icon});

  @override
  State<StatefulWidget> createState() {
    return _SeworkButtonIconState();
  }
}

class _SeworkButtonIconState extends State<SeworkButtonIcon> {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        margin: EdgeInsets.only(
          top: widget.margin,
          right: widget.margin,
        ),
        child: RaisedButton(
            child: Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Icon(
                    widget.icon,
                    size: 20,
                    color: Colors.white,
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: Text(
                    widget.text,
                    style: TextStyle(
                        color: Colors.white, fontWeight: FontWeight.bold),
                  ),
                )
              ],
            ),
            onPressed: widget.onPressed,
            color: widget.color),
      ),
      flex: 1,
    );
  }
}
