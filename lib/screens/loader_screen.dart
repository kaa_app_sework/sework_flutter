import 'package:flutter/material.dart';
import 'package:sework_flutter/theme/sework_colors.dart';
import 'package:sework_flutter/utils/functions.dart';
import 'package:sework_flutter/values/strings.dart';

class LoaderPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _StateLoaderPage();
  }
}

class _StateLoaderPage extends State<LoaderPage> {
  String buttonText;
  bool isClickableButton;
  @override
  void initState() {
    buttonText = strings["loader1"];
    isClickableButton = false;
    Future.delayed(Duration(seconds: 3), () {
      setState(() {
        buttonText = strings["loader2"];
        isClickableButton = true;
        print("is clickable button = $isClickableButton");
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: hexToColor(seworkColors['blue']),
      body: renderBody(),
    );
  }

  Widget renderBody() {
    return Container(
      height: double.maxFinite,
      child: Stack(
        //alignment:new Alignment(x, y)
        children: <Widget>[
          Positioned(
            child: Align(
              alignment: FractionalOffset.center,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Image.asset(
                    "assets/worker.png",
                    height: 150,
                    width: 150,
                  ),
                  Padding(
                    padding: EdgeInsets.all(24),
                    child: Text(
                      strings['slogan'],
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 32,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Positioned(
            child: Align(
                alignment: FractionalOffset.bottomCenter,
                child: Padding(
                  padding: EdgeInsets.all(24),
                  child: FlatButton(
                      color: hexToColor(seworkColors["darkblue"]),
                      shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.white, width: 2),
                          borderRadius: BorderRadius.circular(12)),
                      onPressed: isClickableButton ?  () {
                        Navigator.pushReplacementNamed(context, "/login");
                      } : () {} ,
                      child: Padding(
                        padding: const EdgeInsets.only(
                            top: 15, right: 50, left: 50, bottom: 15),
                        child: Text(
                          buttonText,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 24,
                              color: Colors.white),
                        ),
                      )),
                )),
          )
        ],
      ),
    );
  }
}
