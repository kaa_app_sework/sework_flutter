import 'package:flutter/material.dart';
import 'package:sework_flutter/theme/sework_colors.dart';
import 'package:sework_flutter/utils/functions.dart';
import 'package:sework_flutter/widgets/form.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class LoginScreen extends StatefulWidget {
  @override
  LoginScreenState createState() {
    return new LoginScreenState();
  }
}

class LoginScreenState extends State<LoginScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: hexToColor(seworkColors['blue']),
      body: renderBody(),
    );
  }

  Widget renderBody() {
    double globalFormMargin = 8;

    return Stack(children: <Widget>[
      Positioned(
        child: Align(
          alignment: FractionalOffset.center,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Image.asset(
                "assets/connected.png",
                height: MediaQuery.of(context).size.height / 7,
              ),
              Padding(
                  padding: EdgeInsets.all(24),
                  child: Card(
                    child: Padding(
                        padding: EdgeInsets.all(28),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Container(
                                margin: EdgeInsets.only(
                                    top: globalFormMargin,
                                    bottom: globalFormMargin,
                                    right: globalFormMargin),
                                child: TextFormField(
                                  decoration: InputDecoration(
                                      icon: Icon(FontAwesomeIcons.envelope),
                                      labelText: "Email"),
                                )),
                            Container(
                                margin: EdgeInsets.only(
                                    top: globalFormMargin,
                                    bottom: globalFormMargin,
                                    right: globalFormMargin),
                                child: TextFormField(
                                  obscureText: true,
                                  decoration: InputDecoration(
                                      labelText: 'Mot de passe',
                                      icon: Icon(FontAwesomeIcons.lockOpen)),
                                )),
                            Row(
                              children: <Widget>[
                                SeworkButton(
                                  color: hexToColor(seworkColors["green"]),
                                  margin: globalFormMargin,
                                  onPressed: () {},
                                  text: "Connexion",
                                ),
                                SeworkButton(
                                  color: hexToColor(seworkColors["green"]),
                                  margin: globalFormMargin,
                                  onPressed: () {},
                                  text: "A l'aide !",
                                ),
                              ],
                            ),
                            Row(children: <Widget>[
                              SeworkButtonIcon(
                                color: hexToColor(seworkColors["red"]),
                                icon: FontAwesomeIcons.google,
                                onPressed: () {},
                                text: "Se connecter via Google",
                                margin: globalFormMargin,
                              ),
                            ]),
                            Row(
                              children: <Widget>[
                                SeworkButtonIcon(
                                  color: hexToColor(seworkColors["lightblue"]),
                                  icon: FontAwesomeIcons.facebookF,
                                  onPressed: () {},
                                  text: "Se connecter via Facebook",
                                  margin: globalFormMargin,
                                ),
                              ],
                            ),
                            Container(
                                margin: EdgeInsets.only(
                                  top: globalFormMargin,
                                ),
                                child: Row(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 1,
                                      child: Text(
                                        "- Ou plutôt s'inscrire -",
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  ],
                                )),
                            Row(
                              children: <Widget>[
                                SeworkButton(
                                  margin: globalFormMargin,
                                  color: hexToColor(seworkColors["blue"]),
                                  onPressed: () {
                                    Navigator.of(context).pushNamed('/signup');
                                  },
                                  text: "S'inscrire !",
                                )
                              ],
                            )
                          ],
                        )),
                  ))
            ],
          ),
        ),
      )
    ]);
  }
}
