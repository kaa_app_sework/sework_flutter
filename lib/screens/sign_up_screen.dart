import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:sework_flutter/theme/sework_colors.dart';
import 'package:sework_flutter/utils/functions.dart';
import 'package:sework_flutter/widgets/form.dart';

class SignUpScreen extends StatefulWidget {
  const SignUpScreen({
    Key key,
  }) : super(key: key);

  @override
  SignUpScreenState createState() {
    return new SignUpScreenState();
  }
}

class SignUpScreenState extends State<SignUpScreen> {
  bool hasBeenChosen = false;
  bool isEnterprise = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: hexToColor(seworkColors['blue']),
      appBar: AppBar(
        elevation: 0,
        backgroundColor: hexToColor(seworkColors['blue']),
        leading: Container(
          margin: EdgeInsets.only(left: 12, top: 6),
          child: IconButton(
            icon: Icon(
              FontAwesomeIcons.arrowAltCircleLeft,
              size: 40,
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ),
      ),
      body: renderBody(),
    );
  }

  Widget renderBody() {
    return Stack(children: <Widget>[
      Positioned(
          child: Align(
        alignment: FractionalOffset.center,
        child: Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(16),
              child: Card(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(8),
                      child: Row(
                        children: <Widget>[
                          SeworkButtonIcon(
                            color: hexToColor(seworkColors["blue"]),
                            text: "Je suis un(e) professionel(le)",
                            icon: FontAwesomeIcons.hammer,
                            onPressed: () {
                              print("pressed enterprise button");
                              setState(() {
                                hasBeenChosen = true;
                                isEnterprise = true;
                              });
                            },
                            margin: 0,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.all(8),
                      child: Row(
                        children: <Widget>[
                          SeworkButtonIcon(
                            color: hexToColor(seworkColors["blue"]),
                            text: "Je suis un(e) particulier(e)",
                            icon: FontAwesomeIcons.userAlt,
                            onPressed: () {
                              print("pressed userButton");
                              setState(() {
                                hasBeenChosen = true;
                                isEnterprise = false;
                              });
                            },
                            margin: 0,
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            signUpContent()
          ],
        ),
      )),
    ]);
  }

  Widget signUpContent() {
    print("invoke SignUpContent");
    if (hasBeenChosen && isEnterprise) {
      return enterpriseSignUp();
    } else if (hasBeenChosen && !isEnterprise) {
      return personSignUp();
    } else {
      return Container(
        child: Card(),
      );
    }
  }

  /*Widget renderSignUpInfo(){
    Widget content = Container();

    if (hasBeenChosen && isEnterprise)
      content = enterpriseSignUp();
    else if (hasBeenChosen && !isEnterprise) content = personSignUp();
  }*/
  Widget personSignUp() {
    print("invoke personSignUp()");
    var globalFormMargin = 16.0;
    return Padding(
      padding: EdgeInsets.only(left: 16, right: 16),
      child: Card(
        child: Container(
            padding: EdgeInsets.all(16),
            child: Column(
              children: <Widget>[
                Container(
                    margin: EdgeInsets.only(
                        bottom: globalFormMargin, right: globalFormMargin),
                    child: TextFormField(
                      decoration:
                          InputDecoration(labelText: "Nom de l'entreprise"),
                    )),
              ],
            )),
      ),
    );
  }

  Widget enterpriseSignUp() {
    print("invoke enterpriseSignUp()");

    var globalFormMargin = 16.0;
    return Padding(
      padding: EdgeInsets.only(left: 16, right: 16),
      child: Card(
        child: Container(
            padding: EdgeInsets.all(16),
            child: Column(
              children: <Widget>[
                Container(
                    margin: EdgeInsets.only(
                        bottom: globalFormMargin, right: globalFormMargin),
                    child: TextFormField(
                      decoration:
                          InputDecoration(labelText: "Nom de l'entreprise"),
                    )),
              ],
            )),
      ),
    );
  }
}
