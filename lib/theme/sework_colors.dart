final Map<String,String> seworkColors = {
  "blue" : "#0052cc",
  "darkblue" : "#091e42",
  "lightblue": "#407ae4",
  "red" : "#dc1616",
  "green" : "#006644",
  "yellow" : "#fbd734",
};